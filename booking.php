<?php
session_start();
use \PhpPot\Service\StripePayment;
require_once "config.php";
if(!empty($_POST["token"])) {
    require_once 'StripePayment.php';
    $stripePayment = new StripePayment();
    $stripeResponse = $stripePayment->chargeAmountFromCard($_POST);    
    require_once "DBController.php";
	$arr	 = json_encode($_POST);
	
	
	if(!empty($_POST['date']) && $_POST['date'] == '2019-06-01'){
		//echo '<pre>';print_r($_POST);echo '</pre>';exit;
	}
	
	
    $dbController = new DBController();    
    $amount = $stripeResponse["amount"] /100;    
    $param_type = 'ssdssss';
    $param_value_array = array(
        $_POST['email'],
        $_POST['item_number'],
        $amount,
        $stripeResponse["currency"],
        $stripeResponse["balance_transaction"],
        $stripeResponse["status"],
        json_encode($stripeResponse),
		$arr
    );
    $query = "INSERT INTO tbl_payment (email, item_number, amount, currency_code, txn_id, payment_status, payment_response, frmdata) values (?, ?, ?, ?, ?, ?, ?, ?)";
    $id = $dbController->insert($query, $param_type, $param_value_array);
    
	
	$conn = mysqli_connect("localhost","gpcleaners","Idea@345#upticks","gpclean");
	$query = "INSERT INTO tbl_payment (email, item_number, amount, currency_code, txn_id, payment_status, payment_response, frmdata) 
	values ('".$_POST['email']."',
        '".$_POST['item_number']."',
        '".$amount."',
        '".$stripeResponse["currency"]."',
        '".$stripeResponse["balance_transaction"]."',
        '".$stripeResponse["status"]."',
        '".json_encode($stripeResponse)."',
		'".$arr."')";
	//echo $query;exit;
	mysqli_query($conn,$query);
	
	
    if ($stripeResponse['amount_refunded'] == 0 && empty($stripeResponse['failure_code']) && $stripeResponse['paid'] == 1 && $stripeResponse['captured'] == 1 && $stripeResponse['status'] == 'succeeded') {
        $successMessage = "GP Home Cleaners Booking Completed Successfully. <br>The TXN ID is " . $stripeResponse["balance_transaction"];
		$_SESSION['successMessage']	= $successMessage;//exit;
		$_SESSION['msg'] = 'hello';
		$url = 'https://gphomecleaners.com/booking-completed.php?successMessage='.base64_encode($successMessage);
		header("Location:".$url);
		exit;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Grand Praire Home Cleaners — Booking</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/font-awesome.min.css" />
  <!-- Simple Line Font -->
  <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />  
  <link rel="stylesheet" href="css/datepicker.min.css" />
  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <!--============================= HEADER =============================-->
  <header class="header-style2 fixed-top">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <a href="index.html"><img src="images/logo.png" alt="logo" height="60"/></a><span>469-251-7682</span>
        </div>
      </div>
    </div>
  </header>
  <!--//END HEADER -->
  <!--============================= BOOKING SECTION =============================-->
  <section class="booking-details center-block main-block">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 style="padding-top:2%;">Book your cleaning</h2>
          <h6>
            Its time to book our cleaning service for your home or apartment.
          </h6>
        </div>
      </div>
      <div class="row mt-5">
        <div class="col-md-8 set-sm-fit mb-4">

		
 
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>  

	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script>
		function cardValidation () {
			var valid 		= true;
			var name 		= $('#name').val();
			var email 		= $('#email').val();
			var cardNumber 	= $('#card-number').val();
			var month 		= $('#month').val();
			var year 		= $('#year').val();
			var cvc 		= $('#cvc').val();

			$("#error-message").html("").hide();

			if (name.trim() == "") {
				valid = false;
			}
			if (email.trim() == "") {
				   valid = false;
			}
			if (cardNumber.trim() == "") {
				   valid = false;
			}

			if (month.trim() == "") {
					valid = false;
			}
			if (year.trim() == "") {
				valid = false;
			}
			if (cvc.trim() == "") {
				valid = false;
			}

			if(valid == false) {
				$("#error-message").html("All Fields are required").show();
			}

	if($('#timer1').prop("checked") == false && $('#timer2').prop("checked") == false && $('#timer3').prop("checked") == false && $('#timer5').prop("checked") == false && $('#timer6').prop("checked") == false && $('#timer7').prop("checked") == false){
		$('#timetxt').html('Please select time.');
		$('#timetxt').show();
	}			
			
			return valid;
		}
		//set your publishable key
		Stripe.setPublishableKey("<?php echo STRIPE_PUBLISHABLE_KEY; ?>");

		//callback to handle the response from stripe
		function stripeResponseHandler(status, response) {
			if (response.error) {
				//enable the submit button
				$("#submit-btn").show();
				$( "#loader" ).css("display", "none");
				//display the errors on the form
				$("#error-message").html(response.error.message).show();
				return false;
			} else {
				//get token id
				var token = response['id'];
				//insert the token into the form
				$("#frmStripePayment").append("<input type='hidden' name='token' value='" + token + "' />");
				//submit form to the server
				$("#frmStripePayment").submit();
			}
		}
		function stripePay(e) {
			
			var valid = true;
			var phzip = 0;
			/*var ph 			= $('#phone').val();
			var phlength 	= ph.length;
			if(phlength != 10){
				$('#phone').focus();
				//$('#name-msg').html('Enter valid CVC Number.');
				valid = false;
			}
			
			
			var zi			= $('#zi').val();
			var ziplength 	= zi.length;
			
			if(ziplength != 5){
				$('#zip').focus();
				//$('#name-msg').html('Enter valid CVC Number.');
				valid = false;
			}
			alert();
			alert(phlength+'......'+ziplength);
			return false;*/
			/*$('#card-number').val('');*/
		/*	
			
$('#card-number-msg').html('');
$('#card-number-msg').html('');
$('#cvc-msg').html('');
$('#name-msg').html('');
$('#name-msg').html('');
$('#name-msg').html('');
$('#name-msg').html('');
$('#name-msg').html('');
$('#name-msg').html('');		*/	
			
			
			$('#card-number-msg').html(' ');
			$('#card-number-msg').html(' ');
			$('#cvc-msg').html(' ');
			$('#name-msg').html(' ');
			$('#pnametxt').html(' ');
			$('#emailtxt').html(' ');
			$('#phonetxt').html(' ');
			$('#customersAddresstxt').html(' ');
			$('#ziptxt').html(' ');
		
			
			
			if($('#setDate').val() == ''){
				$('#datemsg').focus();
				$('#card-number-msg').focus();
				$('#card-setDate').html('Enter Date.');
				valid = false;
				
			}
			if($('#card-number').val() == ''){
				$('#card-number').focus();
				$('#card-number-msg').focus();
				$('#card-number-msg').html('Enter valid card Number.');
				valid = false;
				//return false;
			}
			if($('#cvc').val() == ''){
				//alert('Enter valid CVC Number.');
				$('#cvc').focus();
				$('#cvc-msg').html('CVC Number.');
				valid = false;
				//return false;
			}
			if($('#name').val() == ''){
				$('#name').focus();
				$('#name-msg').html('Enter Card Holder\'s Name.');
				valid = false;
				//return false;
			}
			if($('#pname').val() == ''){
				$('#pname').focus();
				$('#pnametxt').html('Enter Full Name.');
				valid = false;
				//return false;
			}
			if($('#email').val() == ''){
				$('#email').focus();
				$('#emailtxt').html('Enter email id.');
				valid = false;
				//return false;
			}
			if($('#phone').val() == ''){
				$('#phone').focus();
				$('#phonetxt').html('Enter phone number.');
				valid = false;
				//return false;
			}
			if($('#customersAddress').val() == ''){
				$('#customersAddress').focus();
				$('#customersAddresstxt').html('Enter address.');
				valid = false;
				//return false;
			}else{
				$('#addresstxt').html('Address : '+$('#customersAddress').val());
			}
$('#hearsource_txt').html(' ');
$('#accesshome_txt').html(' ');
$('#carpark_txt').html(' ');
if($('#hearsource').val() == ''){
	$('#hearsource').focus();
	$('#hearsource_txt').html('Select source.');
	valid = false;
}
if($('#accesshome').val() == ''){
	$('#accesshome').focus();
	$('#accesshome_txt').html('Select access method.');
	valid = false;
}
if($('#carpark').val() == ''){
	$('#carpark').focus();
	$('#carpark_txt').html('Select car parking zone.');
	valid = false;
}


			
			
			
			if($('#zip').val() == ''){
				$('#zip').focus();
				$('#ziptxt').html('Enter zip.');
				valid = false;
				return false;
			}
			
			
				
			if($('#phone').val().length != 10){
				$('#phonetxt').html('Enter phone number with length 10.');
				$('#phone').val('');
				return false;
			}
			if($('#zip').val().length != 5){
				$('#ziptxt').html('Enter zip with length 5.');
				$('#zip').val('');
				return false;
			}
			
			
			
			if(!valid){
				return false;
			}
			
			
			
			e.preventDefault();
			
			//return false;
			
			
			
			var valid = cardValidation();

			if(valid == true) {
				$("#submit-btn").hide();
				$( "#loader" ).css("display", "inline-block");
				Stripe.createToken({
					number: 	$('#card-number').val(),
					cvc: 		$('#cvc').val(),
					exp_month: 	$('#month').val(),
					exp_year: 	$('#year').val()
				}, stripeResponseHandler);

				//submit from callback
				return false;
			}
		}
	</script>



<form id="frmStripePayment" action=""  method="post" autocomplete="off">
	
	


            <div class="preference-title">
              <h4>Cleaning Preferences</h4>
            </div>
			<!-- preferences Wrap -->
            <div class="preferences">
				<?php	if(!empty($successMessage)){	?>
					<div id="success-message"><?php echo $successMessage; ?></div>
				<?php  }	?>
              <!-- Styled radio btn 1 -->
              <div class="preference-radio">
                <p>What type of cleaning?</p>
                <div class="row">
                  <div class="col-xs-12 col-md-4 sm-box">
                    <div class="styled-radio">
                      <input type="radio" id="option-one" name="selector" value="Standard" checked/>
                      <label for="option-one" id="labelOne">REGULAR</label>
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-4 sm-box">
                    <div class="styled-radio">
                      <input type="radio" id="option-two" name="selector" value="Deep" />
                      <label for="option-two" id="labelTwo">DEEP</label>
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-4 sm-box">
                    <div class="styled-radio">
                      <input type="radio" id="option-three" name="selector" value="Move" />
                      <label for="option-three" id="labelThree">MOVE IN/OUT</label>
                    </div>
                  </div>
                </div>
              </div>
              <!--// Styled radio btn 1 -->
              <!-- Styled radio btn 2 -->
              <div class="preference-radio mt-4">
                <p>How often would you like cleaning?</p>
                <div class="row">
                  <div class="col-md-3 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="one-time" name="selector1" value="oneTime" />
                      <label for="one-time" id="labelFour">ONE TIME</label>
                    </div>
                  </div>
                  <div class="col-md-3 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="weekly" name="selector1" value="weekly" checked/>
                      <label for="weekly" class="radio-offer" data-toggle="tooltip" data-placement="bottom"
                        title="SAVE UP TO 10%" id="labelFive">WEEKLY</label>
                      <span><i class="fa fa-star" aria-hidden="true"></i></span>
					  
                    </div>
                  </div>
                  <div class="col-md-3 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="bi-weekly" name="selector1" value="biWeekly" />
                      <label for="bi-weekly" data-toggle="tooltip" data-placement="bottom"
                      title="SAVE UP TO 5%" id="labelSix">BI-WEEKLY</label>
                    </div>
                  </div>
                  <div class="col-md-3 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="monthly" name="selector1" value="monthly" />
                      <label for="monthly" data-toggle="tooltip" data-placement="bottom"
                      title="SAVE UP TO 2%" id="labelSeven">MONTHLY</label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- //Styled radio btn 2 -->
              <div class="row">
                <!-- Home about -->
                <div class="col-md-12">
                  <div class="preference-about_home">
                    <h4>Tell us about your home</h4>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5">
                  <div class="preference-icon">
                    <img src="images/double-bed.svg" />
                    <p>BEDROOMS</p>
                  </div>
                  <div class="preference-quantity">
                    <div class="counter js-counter">
                      <div class="counter__item">
                        <a class="counter__minus js-counter-btn fa fa-minus" aria-hidden="true" id="minus" data-action="minus"></a>
                      </div>
                      <div class="counter__item counter__item--center">
                        <input class="counter__value js-counter-value" type="text" id="counterOne" name="bedrooms" value="1" tabindex="-1" min="0" max="6" required />						
                      </div>
                      <div class="counter__item">
                        <a class="counter__plus js-counter-btn fa fa-plus" aria-hidden="true" id="plus" data-action="plus"></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="preference-icon">
                    <img src="images/shower.svg" />
                    <p>BATHROOMS</p>
                  </div>
                  <div class="preference-quantity">
                    <div class="counter js-counter">
                      <div class="counter__item">
                        <a class="counter__minus js-counter-btn fa fa-minus" id="minus1" aria-hidden="true"
                          data-action="minus"></a>
                      </div>
                      <div class="counter__item counter__item--center">
                        <input class="counter__value js-counter-value" id="counterTwo" name="bathrooms" type="text" value="1" tabindex="-1" min="0" max="10" required />
                      </div>
                      <div class="counter__item">
                        <a class="counter__plus js-counter-btn fa fa-plus" id="plus1" aria-hidden="true" data-action="plus"></a>
                      </div>
                    </div>
                  </div>
                </div>
				<div class="col-md-5">
                <div class="preference-icon" id="l-box-2">
				  <span style="color:#d4d4d4">E.g. Office, extra sitting-room, game room, theater room, study room</span>
                  <img src="images/blank.svg.png" style="display:none;" />
                  <p>Other Rooms</p>
                </div>			  
				<div class="preference-quantity">
					<div class="counter js-counter">
						<div class="counter__item">
							<a class="counter__minus js-counter-btn fa fa-minus" aria-hidden="true" id="minus4" data-action="minus"></a>
						</div>
						<div class="counter__item counter__item--center">
							<input class="counter__value js-counter-value" type="text" id="counterFour" name="otherrooms" value="0" tabindex="-1" min="0" max="6" required />						
						</div>
						<div class="counter__item">
							<a class="counter__plus js-counter-btn fa fa-plus" aria-hidden="true" id="plus4" data-action="plus"></a>
						</div>
					</div>
				</div>			  
			   </div>	
			  
              </div>
              <!--// Home about -->
              <div class="preference-radio mt-4">
                <!-- Styled Check box -->
                <p>Need any extras?</p>
                <?php /*<p>
                  <small>Deep cleaning includes fridge & oven cleaning, so leave
                    those unchecked if you're going for deep clean.</small>
                </p>*/?>
                <div class="row">
                  <div class="col-md-12">
                    <ul class="topics d-flex flex-wrap">
                      <li>
                        <input type="checkbox" id="box-1" name="box-1" value="1" onClick="getPrice()" />
                        <label for="box-1" id="l-box-1" data-toggle="tooltip" data-placement="bottom"  title="Clean inside oven $25" >
						<svg class="checked-svg" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px"
                            viewBox="0 0 463 463" style="enable-background:new 0 0 463 463;" xml:space="preserve"
                            width="65px" height="65px" class="">
                            <g>
                              <g>
                                <g>
                                  <g>
                                    <path
                                      d="M431.5,76h-400C14.131,76,0,90.131,0,107.5v224c0,14.785,10.241,27.216,24,30.591v9.409c0,8.547,6.953,15.5,15.5,15.5h16     c8.547,0,15.5-6.953,15.5-15.5V363h321v8.5c0,8.547,6.953,15.5,15.5,15.5h16c8.547,0,15.5-6.953,15.5-15.5v-9.409     c13.759-3.374,24-15.806,24-30.591v-224C463,90.131,448.869,76,431.5,76z M56,371.5c0,0.275-0.224,0.5-0.5,0.5h-16     c-0.276,0-0.5-0.225-0.5-0.5V363h17V371.5z M31.5,348c-9.098,0-16.5-7.401-16.5-16.5v-224C15,98.401,22.402,91,31.5,91H352v256.5     c0,0.169,0.014,0.334,0.025,0.5H31.5z M424,371.5c0,0.275-0.224,0.5-0.5,0.5h-16c-0.276,0-0.5-0.225-0.5-0.5V363h17V371.5z      M448,331.5c0,9.099-7.402,16.5-16.5,16.5h-64.525c0.011-0.166,0.025-0.331,0.025-0.5V91h64.5c9.098,0,16.5,7.401,16.5,16.5     V331.5z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M327.5,108h-288c-4.142,0-7.5,3.357-7.5,7.5v208c0,4.143,3.358,7.5,7.5,7.5h288c4.142,0,7.5-3.357,7.5-7.5v-208     C335,111.357,331.642,108,327.5,108z M320,316H47V123h273V316z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M415.5,124h-16c-8.547,0-15.5,6.953-15.5,15.5v16c0,8.547,6.953,15.5,15.5,15.5h16c8.547,0,15.5-6.953,15.5-15.5v-16     C431,130.953,424.047,124,415.5,124z M416,155.5c0,0.275-0.224,0.5-0.5,0.5h-16c-0.276,0-0.5-0.225-0.5-0.5v-16     c0-0.275,0.224-0.5,0.5-0.5h16c0.276,0,0.5,0.225,0.5,0.5V155.5z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M423.5,188h-32c-4.142,0-7.5,3.357-7.5,7.5s3.358,7.5,7.5,7.5h32c4.142,0,7.5-3.357,7.5-7.5S427.642,188,423.5,188z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M423.5,252h-32c-4.142,0-7.5,3.357-7.5,7.5s3.358,7.5,7.5,7.5h32c4.142,0,7.5-3.357,7.5-7.5S427.642,252,423.5,252z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M423.5,220h-32c-4.142,0-7.5,3.357-7.5,7.5s3.358,7.5,7.5,7.5h32c4.142,0,7.5-3.357,7.5-7.5S427.642,220,423.5,220z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M168,240.866c0,13.16,5.269,20.18,9.502,25.819c3.772,5.026,6.498,8.658,6.498,16.814c0,4.143,3.358,7.5,7.5,7.5     c4.142,0,7.5-3.357,7.5-7.5c0-13.159-5.269-20.179-9.501-25.818c-3.773-5.026-6.499-8.658-6.499-16.815     c0-8.168,2.727-11.804,6.5-16.835c4.232-5.644,9.5-12.668,9.5-25.836c0-13.173-5.267-20.2-9.498-25.848     c-3.775-5.036-6.502-8.676-6.502-16.853c0-4.143-3.358-7.5-7.5-7.5c-4.142,0-7.5,3.357-7.5,7.5     c0,13.174,5.267,20.201,9.499,25.849c3.774,5.036,6.501,8.676,6.501,16.852c0,8.168-2.727,11.804-6.5,16.836     C173.268,220.676,168,227.699,168,240.866z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M208.484,248.358c2.473,0,4.668-1.198,6.036-3.046c5.37-7.161,16.48-22.023,16.48-47.116     c0-10.194-1.838-19.807-5.463-28.568c-1.584-3.828-5.972-5.648-9.798-4.065c-3.828,1.584-5.646,5.971-4.063,9.798     c2.869,6.935,4.324,14.617,4.324,22.834c0,20.041-8.375,31.307-13.438,38.059c-0.989,1.271-1.578,2.868-1.578,4.604     C200.984,245.001,204.342,248.358,208.484,248.358z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                    <path
                                      d="M148.324,273.877c0.949,0,1.914-0.182,2.845-0.563c3.832-1.573,5.663-5.955,4.09-9.787     c-2.826-6.884-4.259-14.509-4.259-22.661c0-20.137,8.458-31.415,13.512-38.153c0.138-0.184,0.267-0.374,0.387-0.569     c1.484-2.41,1.476-5.506-0.008-7.916c-2.17-3.528-6.79-4.628-10.318-2.458c-1.064,0.655-1.908,1.533-2.507,2.537     c-5.507,7.361-16.066,22.1-16.066,46.559c0,10.116,1.811,19.657,5.383,28.358C142.573,272.123,145.372,273.877,148.324,273.877z"
                                      data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                      fill="#e2e6e9" />
                                  </g>
                                </g>
                              </g>
                            </g>
                          </svg>
                          CLEAN OVEN</label>
                      </li>
                      <li>
                        <input type="checkbox" id="box-2" name="box-2" value="1" onClick="getPrice()" />
                        <label for="box-2" id="l-box-2" data-toggle="tooltip" data-placement="bottom"  title="Clean inside window glass $40">
						
						<svg class="checked-svg" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px"
                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"
                            width="65px" height="65px" class="">
                            <g>
                              <g>
                                <g>
                                  <path
                                    d="M486.794,454.344h-22.919V10.199C463.875,4.567,459.309,0,453.676,0H58.324c-5.633,0-10.199,4.567-10.199,10.199v444.145    H25.206c-5.633,0-10.199,4.567-10.199,10.199v37.258c0,5.632,4.566,10.199,10.199,10.199h461.587    c5.633,0,10.199-4.567,10.199-10.199v-37.258C496.993,458.91,492.427,454.344,486.794,454.344z M68.523,20.398h374.953v433.946    H68.523V20.398z M476.594,491.602H35.406v-16.859h441.189V491.602z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M237.371,40.363H95.583c-5.633,0-10.199,4.567-10.199,10.199v375.687c0,5.632,4.566,10.199,10.199,10.199h141.788    c5.633,0,10.199-4.567,10.199-10.199V50.563C247.57,44.931,243.004,40.363,237.371,40.363z M105.782,60.762h99.842    c-3.257,61.391-19.51,215.545-99.842,300.493V60.762z M105.782,416.05v-26.456c43.97-38.766,76.845-98.796,97.742-178.539    c15.624-59.618,20.82-116.633,22.529-150.293h1.118V416.05H105.782z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M171.134,78.656c-5.633,0-10.199,4.567-10.199,10.199v31.048c0,5.632,4.566,10.199,10.199,10.199    c5.633,0,10.199-4.567,10.199-10.199V88.855C181.334,83.223,176.766,78.656,171.134,78.656z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M133.876,77.621c-5.633,0-10.199,4.567-10.199,10.199v133.508c0,5.632,4.566,10.199,10.199,10.199    c5.633,0,10.199-4.567,10.199-10.199V87.82C144.075,82.188,139.509,77.621,133.876,77.621z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M416.417,40.363H274.629c-5.633,0-10.199,4.567-10.199,10.199v375.687c0,5.632,4.566,10.199,10.199,10.199h141.788    c5.633,0,10.199-4.567,10.199-10.199V50.563C426.616,44.931,422.05,40.363,416.417,40.363z M406.217,416.05H284.828V60.762h1.118    c1.709,33.66,6.906,90.675,22.529,150.293c20.898,79.742,53.773,139.772,97.742,178.539V416.05z M406.218,361.255    c-80.332-84.948-96.585-239.102-99.842-300.493h99.842V361.255z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M340.866,78.656c-5.633,0-10.199,4.567-10.199,10.199v31.048c0,5.632,4.566,10.199,10.199,10.199    c5.633,0,10.199-4.567,10.199-10.199V88.855C351.065,83.223,346.499,78.656,340.866,78.656z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M378.124,205.956c-5.633,0-10.199,4.567-10.199,10.199v5.174c0,5.632,4.566,10.199,10.199,10.199    s10.199-4.567,10.199-10.199v-5.174C388.323,210.522,383.757,205.956,378.124,205.956z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                              <g>
                                <g>
                                  <path
                                    d="M378.124,77.621c-5.633,0-10.199,4.567-10.199,10.199v95.215c0,5.632,4.566,10.199,10.199,10.199    s10.199-4.567,10.199-10.199V87.82C388.323,82.188,383.757,77.621,378.124,77.621z"
                                    data-original="#000000" class="active-path" data-old_color="#e2e6e9"
                                    fill="#e2e6e9" />
                                </g>
                              </g>
                            </g>
                          </svg>CLEAN WINDOWS</label>
                      </li>
                      <li>
                        <input type="checkbox" id="box-3" name="box-3" value="1" onClick="getPrice()" />
                        <label for="box-3" id="l-box-3" data-toggle="tooltip" data-placement="bottom"  title="Clean inside fridge $25">
						
						<svg class="checked-svg" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                            viewBox="0 0 439.5 439.5" style="enable-background:new 0 0 439.5 439.5;"
                            xml:space="preserve" width="65px" height="65px">
                            <g>
                              <g>
                                <path
                                  d="M295.595,0H143.905c-17.645,0-32,14.355-32,32v399.5c0,4.418,3.582,8,8,8h199.689c4.418,0,8-3.582,8-8V32   C327.595,14.355,313.239,0,295.595,0z M127.905,423.5V280.25h136.023c4.418,0,8-3.582,8-8s-3.582-8-8-8H127.905V47.75h136.023   c4.418,0,8-3.582,8-8s-3.582-8-8-8h-80.097c-0.133-4.301-3.654-7.75-7.987-7.75s-7.854,3.449-7.987,7.75h-8.025   c-0.133-4.301-3.654-7.75-7.987-7.75s-7.854,3.449-7.987,7.75h-15.939c0.135-8.706,7.25-15.75,15.987-15.75h151.689   c8.738,0,15.852,7.044,15.987,15.75h-15.653c-4.418,0-8,3.582-8,8s3.582,8,8,8h15.666v216.5h-15.666c-4.418,0-8,3.582-8,8   s3.582,8,8,8h15.666V423.5H127.905z"
                                  data-original="#000000" class="active-path" data-old_color="#e2e6e9" fill="#e2e6e9" />
                                <path
                                  d="M199.845,295.75h-47.939c-4.418,0-8,3.582-8,8s3.582,8,8,8h47.939c4.418,0,8-3.582,8-8S204.263,295.75,199.845,295.75z"
                                  data-original="#000000" class="active-path" data-old_color="#e2e6e9" fill="#e2e6e9" />
                                <path
                                  d="M199.845,232h-47.939c-4.418,0-8,3.582-8,8s3.582,8,8,8h47.939c4.418,0,8-3.582,8-8S204.263,232,199.845,232z"
                                  data-original="#000000" class="active-path" data-old_color="#e2e6e9" fill="#e2e6e9" />
                              </g>
                            </g>
                          </svg>CLEAN FRIDGE</label>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

			  

              
			  <div class="row">
			  <div class="col-md-5">
                <div class="preference-icon" id="l-box-2" data-toggle="tooltip" data-placement="bottom"  title="1 load, dry and hang or fold $15" >
                  <img src="images/double-bed.svg" />
                  <p>Laundry</p>
                </div>
                <div class="preference-quantity">
                  <div class="counter js-counter">
                    <div class="counter__item">
                      <a class="counter__minus js-counter-btn fa fa-minus" aria-hidden="true" id="minus2" data-action="minus"></a>
                    </div>
                    <div class="counter__item counter__item--center">
                      <input class="counter__value js-counter-value" type="text" id="counterThree" name="laundry" value="0"  tabindex="-1" min="0" max="10" required />
                    </div>
                    <div class="counter__item">
                      <a class="counter__plus js-counter-btn fa fa-plus" aria-hidden="true" id="plus2" data-action="plus"></a>
                    </div>
                  </div>
                </div>
              </div>
			  
			  <div class="col-md-5">
                <div class="preference-icon" data-html="true"  id="l-box-2" data-toggle="tooltip" data-placement="bottom"  title="
					1 room & hallway - $65<br>
					2 room & hallway - $80<br>
					3 room & hallway - $120<br>
					4 room & hallway - $160<br>
					5 room & hallway - $200<br>
					6 room & hallway - $240" >
                  <img src="images/double-bed.svg" />
                  <p>Carpet Cleaning</p>
                </div>
                <div class="preference-quantity">
                  <div class="counter js-counter">
                    <div class="counter__item">
                      <a class="counter__minus js-counter-btn fa fa-minus" aria-hidden="true" id="minus3" data-action="minus"></a>
                    </div>
                    <div class="counter__item counter__item--center">
                      <input class="counter__value js-counter-value" type="text" id="counterFive" name="carpet" value="0"  tabindex="-1" min="0" max="6" required />
                    </div>
                    <div class="counter__item">
                      <a class="counter__plus js-counter-btn fa fa-plus" aria-hidden="true" id="plus3" data-action="plus"></a>
                    </div>
                  </div>
                </div>
              </div><?php /**/?>
			  
              </div>

			<div class="row">
				<div class="col-md-12">		
					<br>	  
					<p>Do you have any pets ?</p>
					<input type="radio" name="pets" id="petsyes" value="1" onClick="getPrice()" /> &nbsp; Yes &nbsp; &nbsp;
					<input type="radio" name="pets" id="petsno" value="0" onClick="getPrice()"  /> &nbsp; No
				</div>
			</div>
			
				<div class="preference-radio mt-4">
					<p>
						Any special requirement or instruction for cleaners ?
						<span class="optional-fade">(optional)</span>
					</p>
					<div class="row">
						<div class="col-md-12">
							<textarea class="optinal-textarea" name="note"></textarea>
						</div>
					</div>
				</div>
				<!--// Comment box -->
				<div class="row">
					<div class="col-md-12">
						<div class="preference-about_home">
							<h4>Choose hours and dates</h4>
						</div>
					</div>
				</div>
              <div class="row">
                <div class="col-md-6">
                  <div class="preference-radio mt-4">
                    <p>Choose cleaning date?</p>
                    <div class="form-group">
                      <div class="col-xs-5 date">
                        <div class="input-group input-append date" id="datePicker">
							<span id="card-setDate" style="color:#F00; float:right;"></span>
							<input type="hidden" id="datemsg" />
							<input type="text" class="form-control" name="date" value="" id="setDate"  required />
							<span class="input-group-addon add-on"><i class="fa fa-calendar"
                              aria-hidden="true"></i></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="preference-radio mt-4" >
                <p>When should we arrive? &nbsp; &nbsp; <span id="timetxt" style="color:#F00; display:none;"><span></p>
                <div class="row">
                  <div class="col-md-4 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="timer1" value="7:00 AM - 8:00 AM" name="selector2" />
                      <label for="timer1" id="labelEight">7:00 AM - 8:00 AM</label>
                    </div>
                  </div>
                  <div class="col-md-4 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="timer2" value="9:00 AM - 10:00 AM" name="selector2" />
                      <label for="timer2" id="labelNine">9:00 AM - 10:00 AM</label>
                    </div>
                  </div>
                  <div class="col-md-4 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="timer3" value="11:00 AM - 12 NOON" name="selector2" />
                      <label for="timer3" id="labelTen">11:00 AM - 12 NOON</label>
                    </div>
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-md-4 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="timer5" value="1:00 PM - 2:00 PM" name="selector2" />
                      <label for="timer5" id="label12">1:00 PM - 2:00 PM</label>
                    </div>
                  </div>
                  <div class="col-md-4 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="timer6" value="3:00 PM - 4:00 PM" name="selector2" />
                      <label for="timer6" id="label13">3:00 PM - 4:00 PM</label>
                    </div>
                  </div>
                  <div class="col-md-4 sm-box">
                    <div class="styled-radio styled-radio2">
                      <input type="radio" id="timer7" value="5:00 PM - 6:00 PM" name="selector2" />
                      <label for="timer7" id="label14">5:00 PM - 6:00 PM</label>
                    </div>
                  </div>
                </div>              			  
			  </div>
				<div class="row">
					<div class="col-md-12">
						<div class="preference-about_home">
							<h4>Additional Information</h4>
						</div>
					</div>
				</div>
				<div class="preference-radio mt-4">
					<p>Where can we park? * &nbsp; &nbsp;<span id="carpark_txt" style="color:#F00; display:none1;"></span></p>
					<div class="row">
						<div class="col-md-12">
							<div class="card-details">
								<select id="carpark"  style="" class="card-number">
									<option value=""></option>
									<option>Park in my drive way</option>
									<option>There is street parking</option>
									<option>Park in the parking garage</option>
									<option>Other, I'll explain below</option>
								</select>
							</div>
						</div>
					</div>			  
				</div>
				<div class="preference-radio mt-4">
					<p>How will we access your home? * &nbsp; &nbsp;<span id="accesshome_txt" style="color:#F00; display:none1;"></span></p>
					<div class="row">
						<div class="col-md-12">
							<div class="card-details">
								<select id="accesshome" class="card-number">
									<option value=""></option>
									<option>I will leave a key</option>
									<option>I will provide an access code</option>
									<option>Someone will be home</option>
									<option>Go to apartment office for key</option>
									<option>Other, I'll explain below</option>
								</select>								
							</div>
						</div>
					</div>			  
				</div>			  
				<div class="preference-radio mt-4">
					<p>How did you hear about Grand Prairie Home Cleaners' with:? * &nbsp; &nbsp;<span id="hearsource_txt" style="color:#F00; display:none1;"></span></p>
					<div class="row">
						<div class="col-md-12">
							<div class="card-details">
								<select id="hearsource" class="card-number" onchange="hearsourcefn()">
									<option value=""></option>
									<option>Google</option>
									<option>Facebook</option>
									<option>Instagram</option>
									<option>Twitter</option>
									<option>Yelp</option>
									<option>Referral</option>
									<option>Other</option>
								</select>
								<br><br>
								<input type="text" id="others_info" name="others_info" class="card-number card-num" placeholder="Others Information" value="" style="display:none;" />
							</div>
						</div>
					</div>			  
				</div>			  
			  
			  
              <div class="row">
                <div class="col-md-12">
                  <div class="preference-about_home">
                    <h4>Payment Method</h4>
                    <div class="payment-icon">
                      <i class="fa fa-lock" aria-hidden="true"></i>
                      <p>
                        256 bit Secure<br />
                        SSL Encryption
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="preference-radio mt-4">
                <p id="error-message" style="color: #f00;font-size: initial;" ></p>
				<!-- Card Details -->
                <p>Credit Card details</p>
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-details">
						<span id="card-number-msg" style="color:#F00;"></span>
						<input type="text" id="card-number" name="card-number" class="card-number card-num" placeholder="card number" value="" required />
                      <?php /*<img src="images/visa.png" class="visa-img" alt="visa-icon" />*/ ?>
                    </div>
                  </div>
                </div>
                
				<div class="row">
					<div class="col-md-6">
						<span id="cvc-msg" style="color:#F00; float:right;"></span>
					</div>
					<div class="col-md-6">
						<span id="name-msg" style="color:#F00;"></span>
					</div>
				</div>
				
				<div class="row mt-3">
                  <div class="col-md-4">
                    <div class="card-details">
						<select name="month" id="month" class="card-number" style="width:45%; padding:20px 10px 20px 5px;" >
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
						<select name="year" id="year" class="card-number" style="width:45%; padding:20px 10px 20px 5px;" >
							<option value="20">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
							<option value="23">2023</option>
							<option value="24">2024</option>
							<option value="25">2025</option>
							<option value="26">2026</option>
							<option value="27">2027</option>
							<option value="28">2028</option>
							<option value="29">2029</option>
							<option value="30">2030</option>
						</select>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="card-details">
						<input type="text" placeholder="CVV"  name="cvc" id="cvc" class="card-number cvv-input" value="" required />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-details">
						
						<input type="text" id="name" name="name" class="card-number" placeholder="Name as on Card" value="" required />
                    </div>
                  </div>
                </div>
              </div>
              <!--// Card Details -->
              <div class="preference-radio mt-5">
                <!-- Personal Details -->
                <p>Pesonal Details</p>
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-details">
						<span id="pnametxt" style="color:#F00;"></span>
                      <input type="text" placeholder="Full Name" class="card-number" id="pname" name="pname" value="" required />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="card-details">
						<span id="emailtxt" style="color:#F00;"></span>
						<input type="email" id="email" name="email" class="card-number" placeholder="Email" value="" />
						<input type="hidden" placeholder="Email Address" class="card-number" id="mail"  />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card-details">
						<span id="phonetxt" style="color:#F00;"></span>
                      <input type="text" placeholder="Phone Number" class="card-number" id="phone" name="phone" minlength="10" maxlength="10" value="<?php if(isset($_POST['phone'])){echo $_POST['phone'];}?>" />
                    </div>
                  </div>
                </div>
                <div class="row mt-3">
                  <div class="col-md-9">
                    <div class="card-details">
					
					<span id="customersAddresstxt" style="color:#F00;"></span>
                      <input type="text" placeholder="Your Full Address" id="customersAddress" name="customersAddress" class="card-number" value="" />
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="card-details">
					<span id="ziptxt" style="color:#F00;"></span>
                      <input type="text" placeholder="10023" class="card-number" id="zip" name="zip" minlength="5" maxlength="5" value="<?php if(isset($_POST['zip'])){echo $_POST['zip'];}?>"  />
                    </div>
                  </div>
                </div>
                <div class="row mt-3 d-none d-md-block">
                  <div class="col-md-12">
                    <div class="terms-reminder">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">I read and agree to the
                          <a href="/terms-conditions.html" target="_blank">terms & conditions</a></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="row mt-4  d-none d-md-block">
                  <div class="col-md-12">
					<div id="loader2" style="display:none;">
						<img alt="loader" src="LoaderIcon.gif">
					</div>
					<button class="btn btn-block complete-booking" type="button" name="pay_now2" value="Submit" id="submit-btn2"  onClick="stripePay(event);"  >
						<span class="pe-7s-unlock"></span>Complete Booking
					</button>	
                    <?php /*
					<input type="submit" name="pay_now" value="Submit" id="submit-btn" class="btn btn-block complete-booking" onClick="stripePay(event);">	
					<button class="btn btn-block complete-booking" type="button" id="sbmt">
                      <span class="pe-7s-unlock"></span>Complete Booking
                    </button>*/ ?>
                  </div>
                </div>
              </div>
              <!--// Personal Details -->
            </div>
            <!--// preferences Wrap -->
                  				
		</div>
        <!-- Booking Summary -->
        <div class="col-md-4 set-sm-fit">
          <div data-toggle="affix">
            <!-- data-toggle="affix" -->
            <div class="preference-title">
              <h4>Booking Summary</h4>
            </div>
            <div class="fesilities">
              <ul>
                <li>
					<i class="fa fa-home" aria-hidden="true"></i> 
					<p id="housesizetxt">Size of House</p>
                </li>
				<li id="otherorderli">
					<i class="fa fa-home" aria-hidden="true"></i> 
					<p id="otherordertxt"></p>
                </li>
				<li>
                  <i class="fa fa-paint-brush" aria-hidden="true"></i>
                  <p id="type1">Standard</p>
                </li>
                <li>
                  <i class="fa fa-calendar" aria-hidden="true"></i>
                  <p id="date"></p>
                </li>
                <li>
                  <i class="fa fa-clock-o" aria-hidden="true"></i>
                  <p id="time">7:00 AM - 8:00 AM</p>
                </li>
                <li>
                  <i class="fa fa-refresh" aria-hidden="true"></i>
                  <p id="often">One time</p>
                </li>
                <li>
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  <p id="addresstxt">Address.</p>
				  <hr>
                </li>
				<li id="bedrooms_li" style="display:none;">
					<p>Bedrooms and Bathrooms: <span style="float:right;" id="bedrooms_txt"></span></p>
				</li>
				<li id="otherrooms_li" style="display:none;">
					<p>Other Rooms : <span style="float:right;" id="otherrooms_txt"></span></p>
				</li>
				<li id="cleanoven_li" style="display:none;">
					<p>Clean Oven : <span style="float:right;" id="cleanoven_txt"></span></p>
				</li>
				<li id="cleanwindows_li" style="display:none;">
					<p>Clean Windows : <span style="float:right;" id="cleanwindows_txt"></span></p>
				</li>
				<li id="cleanfridge_li" style="display:none;">
					<p>Clean Fridge : <span style="float:right;" id="cleanfridge_txt"></span></p>
				</li>
				<li id="laundry_li" style="display:none;">
					<p>Laundry : <span style="float:right;" id="laundry_txt"></span></p>
				</li>
				<li id="pets_li" style="display:none;">
					<p>Pets in house: <span style="float:right;" id="pets_txt"></span></p>
				</li>
				<li id="stotal_li" style="display:none;">
					<p>Sub-Total: <span style="float:right;" id="stotal_txt"></span></p>
				</li>
				<li id="discount_li" style="display:none;">
					<p>Discount: <span style="float:right;" id="discount_txt"></span></p>
				</li>
				<li id="tax_li" style="display:none;">
					<p>Sales Tax: <span style="float:right;" id="tax_txt"></span></p>
				</li>
				
              </ul>
              <h4>Total cost<span id="amount"></span></h4>
            </div>
          </div>
        </div>                
                  <div class="col-md-4 d-block d-md-none">
                    <div class="terms-reminder">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">I read and agree to the
                          <a href="#">terms & conditions</a></span>
                      </label>
                    </div>
                  </div>
                
                  <div class="col-md-12 d-block d-md-none">
					<input type='hidden' name='amount' id='stripe_amount' value='0' > 
					<input type='hidden' name='currency_code' value='USD'> 
					<input type='hidden' name='item_name' value='Test Product <?php echo date('H:i:s');?>'>
					<input type='hidden' name='item_number' value='PHPPOTEG#1'>
					<div id="loader" style="display:none;">
						<img alt="loader" src="LoaderIcon.gif">
					</div>
					<button class="btn btn-block complete-booking" type="submit" name="pay_now" value="Submit" id="submit-btn"  onClick="stripePay(event);"  >
						<span class="pe-7s-unlock"></span>Complete Booking
					</button>	
                    <?php /*
					<input type="submit" name="pay_now" value="Submit" id="submit-btn" class="btn btn-block complete-booking" onClick="stripePay(event);">	
					<button class="btn btn-block complete-booking" type="button" id="sbmt">
                      <span class="pe-7s-unlock"></span>Complete Booking
                    </button>*/ ?>
                  </div>
                

		</form>
		
        <!--// Booking Summary -->
      </div>
    </div>
  </section>
  <!--//END BOOKING SECTION -->
    <!--============================= FOOTER =============================-->
    <footer style="display:none;">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-2">
                    <div class="foot-box">
                        <h6>QUICK LINKS</h6>
                        <ul>
                            <li><a href="#">FREE QUOTE</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">JOIN THE CREW</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="foot-box">
                        <h6>LEGAL STUFF</h6>
                        <ul>
                            <li>Terms of use</li>
                            <li>Cookies</li>
                            <li>Privacy Policy</li>
                            <li>Security Policy</li>
                            <li>Money back Guarantee</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="foot-box">
                        <h6>OUR LOCATIONS</h6>
                        <ul>
							<li>Now serving all DFW locations</li>
                        </ul>
                    </div>
					<div class="foot-box">
						<h6 style="margin-top: 22px">Stay Connected</h6>
						<ul>
							<li><i class="fa fa-mobile fa-2x" aria-hidden="true"></i> &nbsp; 469 251 7682</li>
							<li><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp; services@gphomecleaners.com</li>
						</ul>
					</div>					
                </div>
                <div class="col-md-4">
                    <div class="subscribe">
                        <h6>FOLLOW US</h6>
                        <form class="form-inline" action="booking.html">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <input type="email" class="form-control subscribe-form" id="inlineFormInputGroup" placeholder="Enter your email">
                                <button type="submit" class="input-group-addon subscribe-btn"><span class="pe-7s-angle-right"></span></button>
                            </div>
                        </form>
                        <div class="zsocial-icons" style="margin-top:10px;">
                            <a href="https://www.facebook.com/gpCleanerss/?view_public_for=449639128935832" target="_blank" ><img src="/images/fb.png" style="width:32pt;" /></a>
							<a href="https://twitter.com/CleanersGp" target="_blank"><img src="/images/twitter.png" style="width:26pt;" /></a>
							<a href="https://www.instagram.com/gpcleanerss/" target="_blank"><img src="/images/instagram.png" style="width:26pt;" /></a>
							
							<a href="https://facebook.com" style="display:none;"><img src="/images/ln.png" style="width:26pt;" /></a>
							<a href="https://facebook.com" style="display:none;"><img src="/images/g.png" style="width:26pt;" /></a>
							
						</div>
						
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <a href="#">&copy; 2019 Grand Prairie Home Cleaners. All rights reserved</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	
    <!--//END FOOTER -->
  
  
  
  
  
  <!-- jQuery, Bootstrap JS. -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Slick JS -->
  <script src="js/slick.min.js"></script>
  <!-- Date Picker JS -->
  <script src="js/datepicker.min.js"></script>
  <!-- Main JS -->
  <script src="js/script.js"></script>
  
  <?php if(!empty($_REQUEST['test'])){	?>
	  <!-- booking JS -->
	  <script src="js/newbookingpage06.js"></script>
  <?php }else{	?>
	  <!-- booking JS --> 
	  <script src="js/newbookingpage06.js"></script>	  
  <?php }	?>
<style>
.affix{
	width:22%;
}
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  top: 150%;
  left: 50%;
  margin-left: -60px;
}

.tooltip .tooltiptext::after {
  content: "";
  position: absolute;
  bottom: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: transparent transparent black transparent;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>
</body>
</html>