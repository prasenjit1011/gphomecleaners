var bookingDetails = {
	name: document.getElementById('name').value,
	mail: document.getElementById('mail').value,
	phone: document.getElementById('phone').value,
	address: document.getElementById('customersAddress').value,
	zip: document.getElementById('zip').value,
	type: 'Standard',
	frequency: 'weekly',
	date: $('#setDate').val(),
	time: '',
	beds: parseInt($('#counterOne').val()),
	baths: parseInt($('#counterTwo').val()),
	fridge: document.getElementById('box-3').checked,
	cabinets: false,
	laundry: parseInt($('#counterThree').val()),
	otherrooms: parseInt($('#counterFour').val()),
	windows: document.getElementById('box-2').checked,
	oven: document.getElementById('box-1').checked,
	extras: 0,
	total: 0,
	price: 0
};

function setExtrasInitialValue() {
	let extras;
	extras = bookingDetails.oven ? extras + 17 : 0;
	extras = bookingDetails.windows ? extras + 67 : 0;
	extras = bookingDetails.fridge ? extras + 35 : 0;
	return parseInt(extras);
};

bookingDetails.extras = setExtrasInitialValue();

const pricing = {
  1: { 
    min: 1, 
    max: 1, 
    price: { 
      Standard: {1: 120},
      Deep: {1: 180},
      Move: {1: 230}
    },
  },
  2: { 
    min: 1,
    max: 2,
    price: { 
      Standard: {1: 155, 2: 195},
      Deep: {1: 215, 2:255},
      Move: {1: 265, 2:305}
    },
  },
  3: { 
    min: 1, 
    max: 3, 
    price: {
      Standard: {1: 190, 2: 230, 3: 270},
      Deep: {1: 250, 2:290, 3: 330},
      Move: {1: 300, 2:340, 3: 380}
    },
  },
  4: {
    min: 2,
    max: 5,
    price: { 
      Standard: {2: 265, 3: 305, 4: 345, 5: 385},
      Deep: {2:325, 3: 365, 4: 405, 5: 445},
      Move: {2:375, 3: 415, 4: 455, 5: 495}
    },
  },
  5: {
    min: 3,
    max: 5,
    price: {
      Standard: {3: 340, 4: 380, 5: 420},
      Deep: {3: 400, 4:440, 5:480},
      Move: {3: 450, 4:490, 5:530}
    },
  },
  6: { 
    min: 5, 
    max: 5, 
    price: {
      Standard: {5: 455},
      Deep: {5: 515},
      Move: {5: 565}
    }
  }
};

discounts = {
  oneTime: 0,
  weekly: 15,
  biWeekly: 10,
  monthly: 5
};

//cleaning preferences
var standard 	= $('#labelOne');
var deep 		= $('#labelTwo');
var moveIn 		= $('#labelThree');
var cleanType 	= $('#type1');
var option1 	= $('#option-one').val();
var option2 	= $('#option-two').val();
var option3 	= $('#option-three').val();
// how often
var oneTime = $('#labelFour');
var weekly = $('#labelFive');
var biWeekly = $('#labelSix');
var monthly = $('#labelSeven');
var option4 = $('#one-time').val();
var option5 = $('#weekly').val();
var option6 = $('#bi-weekly').val();
var option7 = $('#monthly').val();
var often = $('#often');

var hour = $('#hours').val();
var time = $('#time');
var increment = $('#add');

// cleaning preference event handlers
standard.on('click', function() {
	bookingDetails.type = option1;
	cleanType.html('REGULAR');
	getPrice();
});

deep.on('click', function() {
	bookingDetails.type = option2;
	cleanType.html('DEEP');
	getPrice();
});

moveIn.on('click', function() {
	bookingDetails.type = option3;
	cleanType.html('MOVE IN/OUT');
	getPrice();
});

// how often
oneTime.on('click', function() {
	bookingDetails.frequency = option4;
	often.html('One Time');
	getPrice();
});

weekly.on('click', function() {
  bookingDetails.frequency = option5
  often.html('Weekly');
  getPrice();
});
biWeekly.on('click', function() {
  bookingDetails.frequency = option6;
  often.html('Bi Weekly');
  getPrice();
});
monthly.on('click', function() {
  bookingDetails.frequency = option7;
  often.html('Monthly');
  getPrice();
});


// Personal Details
var address = $('#customersAddress');
var addressShow = $('#address');

address.on('change', function() {
  bookingDetails.address = address.val();
  addressShow.html(bookingDetails.address);
});

$('#name').on('change', () => {
  bookingDetails.name = $('#name').val();
});

$('#mail').on('change', () => {
  bookingDetails.mail = $('#mail').val();
});

$('#phone').on('change', () => {
  bookingDetails.phone = $('#phone').val();
});

$('#zip').on('change', () => {
  bookingDetails.zip = $('#zip').val();
});


//time label
var time1 = $('#labelEight');
var time2 = $('#labelNine');
var time3 = $('#labelTen');
var time4 = $('#label11');
var time5 = $('#label12');
var time6 = $('#label13');
var time7 = $('#label14');
var time8 = $('#label15');

//time input
var timer1 = $('#timer1').val();
var timer2 = $('#timer2').val();
var timer3 = $('#timer3').val();
var timer4 = $('#timer4').val();
var timer5 = $('#timer5').val();
var timer6 = $('#timer6').val();
var timer7 = $('#timer7').val();
var timer8 = $('#timer8').val();

time1.on('click', function() {
  bookingDetails.time = timer1;
  time.html(bookingDetails.time);
});

time2.on('click', function() {
  bookingDetails.time = timer2;
  time.html(bookingDetails.time);
});

time3.on('click', function() {
  bookingDetails.time = timer3;
  time.html(bookingDetails.time);
});

time4.on('click', function() {
  bookingDetails.time = timer4;
  time.html(bookingDetails.time);
});

time5.on('click', function() {
  bookingDetails.time = timer5;
  time.html(bookingDetails.time);
});

time6.on('click', function() {
  bookingDetails.time = timer6;
  time.html(bookingDetails.time);
});

time7.on('click', function() {
  bookingDetails.time = timer7;
  time.html(bookingDetails.time);
});
time8.on('click', function() {
  bookingDetails.time = timer8;
  time.html(bookingDetails.time);
});

//setting the date
var date = $('#date');
var setDate = $('#setDate');

setDate.on('change', function() {
  bookingDetails.date = setDate.val();
  date.html(bookingDetails.date);
});

//date
const dateNow 	= new Date();
const timeDate 	= dateNow.getHours();
const year 		= dateNow.getFullYear();
const month 	= dateNow.getMonth();
date.html(`${timeDate + 1}/${month}/${year}`);

// Set extras
const ovenLabel = $('#l-box-1');
const windowLabel = $('#l-box-2');
const fridgeLabel = $('#l-box-3');

ovenLabel.on('click', () => {
  bookingDetails.oven = !document.getElementById('box-1').checked;
  bookingDetails.extras = bookingDetails.oven ? bookingDetails.extras + 17 : bookingDetails.extras - 17;
  getPrice();
});

windowLabel.on('click', () => {
  bookingDetails.windows = !document.getElementById('box-2').checked;
  bookingDetails.extras = bookingDetails.windows ? bookingDetails.extras + 67 : bookingDetails.extras - 67;
  getPrice();
});

fridgeLabel.on('click', () => {
  bookingDetails.fridge = !document.getElementById('box-3').checked;
  bookingDetails.extras = bookingDetails.fridge ? bookingDetails.extras + 35 : bookingDetails.extras - 35;
  getPrice();
});

//Tell us about your home
var bedroom = $('#counterOne');
var bath 	= $('#counterTwo');
var laundry	= $('#counterThree');
var otherrooms	= $('#counterFour');

var add 	= $('#plus');
var minus 	= $('#minus');
var add1 	= $('#plus1');
var minus1 	= $('#minus1');
var add2 	= $('#plus2');
var minus2 	= $('#minus2');

var add4 	= $('#plus4');
var minus4 	= $('#minus4');

var total = $('#amount');

add.on('click', ()=>{
  if ($('#counterOne').val() >= 6) {
    bedroom.val(5);
  }
  bookingDetails.beds = parseInt(bedroom.val()) + 1;
  //$('#counterOne').val(bookingDetails.beds);
  //alert($('#counterOne').val());  
  getPrice();
});

minus.on('click', function() {
	if ($('#counterOne').val() <= 1) {
		bedroom.val(2);
	}
	bookingDetails.beds = parseInt(bedroom.val()) - 1;
	//$('#counterOne').val(bookingDetails.beds);
	//alert($('#counterOne').val());
	getPrice();
});




add1.on('click', function() {
	if ($('#counterTwo').val() >= 5) {
		bath.val(4);
	}
	bookingDetails.baths = parseInt(bath.val()) + 1;
	console.log(bookingDetails.baths);
	getPrice();
});

minus1.on('click', function() {
  if ($('#counterTwo').val() <= 1) {
    bath.val(2);
  }
  bookingDetails.baths = parseInt(bath.val()) - 1;
  getPrice();
});



add2.on('click', ()=>{
  if ($('#counterThree').val() >= 6) {
    laundry.val(5);
  }
  bookingDetails.laundry = parseInt(laundry.val()) + 1;
  //$('#counterThree').val(bookingDetails.laundry);
  //alert($('#counterThree').val());  
  getPrice();
});

minus2.on('click', function() {
	if ($('#counterThree').val() <= 1) {
		laundry.val(1);
	}
	bookingDetails.laundry = parseInt(laundry.val()) - 1;
	//$('#counterThree').val(bookingDetails.laundry);
	//alert($('#counterOne').val());
	getPrice();
});




add4.on('click', ()=>{
	if ($('#counterFour').val() >= 6) {
		otherrooms.val(5);
	}
	bookingDetails.otherrooms = parseInt(otherrooms.val()) + 1;
	//$('#counterFour').val(bookingDetails.beds);
	//alert($('#counterFour').val());  
	getPrice();
});

minus4.on('click', function() {
	if ($('#counterFour').val() <= 0) {
		otherrooms.val(1);
	}
	bookingDetails.otherrooms = parseInt(otherrooms.val()) - 1;
	//$('#counterOne').val(bookingDetails.beds);
	//alert($('#counterFour').val());
	getPrice();
});



$('#submit').on('click', () => {
  makeBooking();
});

function getPrice() 
{
	//alert(bookingDetails.type);
	//type 	= bookingDetails.type == 'Move' ? 'Deep' : bookingDetails.type;
	type 	= bookingDetails.type;
	
	nBeds 	= pricing[bookingDetails.beds];
	nBaths 	= bookingDetails.baths;
	
	/*console.log('nbeds :: '+nBeds);
	console.log('nbath :: '+nBaths);
	alert($('#counterOne').val());
	alert($('#counterTwo').val());
	alert($('#counterFour').val());*/
	
	
	pets = $("input[name='pets']:checked").val();
	petsprice = 0;
    if(pets == 1){
		petsprice = 20;
	}	
	
	
	if (nBaths <= nBeds.max) {
		if (nBaths >= nBeds.min) {
			bookingDetails.baths = nBaths;
			bookingDetails.total = nBeds.price[type][nBaths] + bookingDetails.extras + 15*bookingDetails.laundry  + 35*bookingDetails.otherrooms + petsprice;//
			bookingDetails.price = getDiscount();
			total.html(`$${bookingDetails.price}`);
		} else {
			bookingDetails.baths = nBeds.min;
			bookingDetails.total = nBeds.price[type][nBeds.min] + bookingDetails.extras + 15*bookingDetails.laundry + 35*bookingDetails.otherrooms + petsprice;//
			console.log(bookingDetails.total);
			bookingDetails.price = getDiscount();
			total.html(`$${bookingDetails.price}`);
		}
	} else {
		bookingDetails.baths = nBeds.max;
		bookingDetails.total = nBeds.price[type][nBeds.max] + bookingDetails.extras;
		console.log(bookingDetails.total);
		bookingDetails.price = getDiscount();
		total.html(`$${bookingDetails.price}`);
	}
	$('#stripe_amount').val(bookingDetails.price);
}




function getDiscount() {
  return bookingDetails.total - ((discounts[bookingDetails.frequency]*bookingDetails.total) / 100);
}

function makeBooking() {
	console.log(bookingDetails);
	$.ajax({
		url: 'submit.php',
		type: 'post',
		dataType: 'json',
		contentType: 'application/json',
		success: function (data) {
			console.log(data);
		},
		data: JSON.stringify(bookingDetails)
	});
}

getPrice();