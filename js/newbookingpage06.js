var bookingDetails = {
	name: document.getElementById('name').value,
	mail: document.getElementById('mail').value,
	phone: document.getElementById('phone').value,
	address: document.getElementById('customersAddress').value,
	zip: document.getElementById('zip').value,
	type: 'Standard',
	frequency: 'weekly',
	date: $('#setDate').val(),
	time: '',
	beds: parseInt($('#counterOne').val()),
	baths: parseInt($('#counterTwo').val()),
	fridge: document.getElementById('box-3').checked,
	cabinets: false,
	laundry: parseInt($('#counterThree').val()),
	otherrooms: parseInt($('#counterFour').val()),
	windows: document.getElementById('box-2').checked,
	oven: document.getElementById('box-1').checked,
	extras: 0,
	total: 0,
	price: 0
};

function setExtrasInitialValue() {
	let extras;
	extras = bookingDetails.oven ? extras + 17 : 0;
	extras = bookingDetails.windows ? extras + 67 : 0;
	extras = bookingDetails.fridge ? extras + 35 : 0;
	return parseInt(extras);
};

bookingDetails.extras = setExtrasInitialValue();

const pricing = {
  1: { 
    min: 1, 
    max: 1, 
    price: { 
      Standard: {1: 120},
      Deep: {1: 180},
      Move: {1: 230}
    },
  },
  2: { 
    min: 1,
    max: 2,
    price: { 
      Standard: {1: 155, 2: 195},
      Deep: {1: 215, 2:255},
      Move: {1: 265, 2:305}
    },
  },
  3: { 
    min: 1, 
    max: 3, 
    price: {
      Standard: {1: 190, 2: 230, 3: 270},
      Deep: {1: 250, 2:290, 3: 330},
      Move: {1: 300, 2:340, 3: 380}
    },
  },
  4: {
    min: 2,
    max: 5,
    price: { 
      Standard: {2: 265, 3: 305, 4: 345, 5: 385},
      Deep: {2:325, 3: 365, 4: 405, 5: 445},
      Move: {2:375, 3: 415, 4: 455, 5: 495}
    },
  },
  5: {
    min: 3,
    max: 5,
    price: {
      Standard: {3: 340, 4: 380, 5: 420},
      Deep: {3: 400, 4:440, 5:480},
      Move: {3: 450, 4:490, 5:530}
    },
  },
  6: { 
    min: 5, 
    max: 5, 
    price: {
      Standard: {5: 455},
      Deep: {5: 515},
      Move: {5: 565}
    }
  }
};

discounts = {
  oneTime: 0,
  weekly: 15,
  biWeekly: 10,
  monthly: 5
};

//cleaning preferences
var standard 	= $('#labelOne');
var deep 		= $('#labelTwo');
var moveIn 		= $('#labelThree');
var cleanType 	= $('#type1');
var option1 	= $('#option-one').val();
var option2 	= $('#option-two').val();
var option3 	= $('#option-three').val();
// how often
var oneTime = $('#labelFour');
var weekly = $('#labelFive');
var biWeekly = $('#labelSix');
var monthly = $('#labelSeven');
var option4 = $('#one-time').val();
var option5 = $('#weekly').val();
var option6 = $('#bi-weekly').val();
var option7 = $('#monthly').val();
var often = $('#often');

var hour = $('#hours').val();
var time = $('#time');
var increment = $('#add');

// cleaning preference event handlers
standard.on('click', function() {
	bookingDetails.type = option1;
	cleanType.html('REGULAR');
	getPrice();
});

deep.on('click', function() {
	bookingDetails.type = option2;
	cleanType.html('DEEP');
	getPrice();
});

moveIn.on('click', function() {
	bookingDetails.type = option3;
	cleanType.html('MOVE IN/OUT');
	getPrice();
});

// how often
oneTime.on('click', function() {
	bookingDetails.frequency = option4;
	often.html('One Time');
	getPrice();
});

weekly.on('click', function() {
  bookingDetails.frequency = option5
  often.html('Weekly');
  getPrice();
});
biWeekly.on('click', function() {
  bookingDetails.frequency = option6;
  often.html('Bi Weekly');
  getPrice();
});
monthly.on('click', function() {
  bookingDetails.frequency = option7;
  often.html('Monthly');
  getPrice();
});


// Personal Details
var address = $('#customersAddress');
var addressShow = $('#address');

address.on('change', function() {
  bookingDetails.address = address.val();
  addressShow.html(bookingDetails.address);
});

$('#name').on('change', () => {
  bookingDetails.name = $('#name').val();
});

$('#mail').on('change', () => {
  bookingDetails.mail = $('#mail').val();
});

$('#phone').on('change', () => {
  bookingDetails.phone = $('#phone').val();
});

$('#zip').on('change', () => {
  bookingDetails.zip = $('#zip').val();
});


//time label
var time1 = $('#labelEight');
var time2 = $('#labelNine');
var time3 = $('#labelTen');
var time4 = $('#label11');
var time5 = $('#label12');
var time6 = $('#label13');
var time7 = $('#label14');
var time8 = $('#label15');

//time input
var timer1 = $('#timer1').val();
var timer2 = $('#timer2').val();
var timer3 = $('#timer3').val();
var timer4 = $('#timer4').val();
var timer5 = $('#timer5').val();
var timer6 = $('#timer6').val();
var timer7 = $('#timer7').val();
var timer8 = $('#timer8').val();

time1.on('click', function() {
  bookingDetails.time = timer1;
  time.html(bookingDetails.time);
});

time2.on('click', function() {
  bookingDetails.time = timer2;
  time.html(bookingDetails.time);
});

time3.on('click', function() {
  bookingDetails.time = timer3;
  time.html(bookingDetails.time);
});

time4.on('click', function() {
  bookingDetails.time = timer4;
  time.html(bookingDetails.time);
});

time5.on('click', function() {
  bookingDetails.time = timer5;
  time.html(bookingDetails.time);
});

time6.on('click', function() {
  bookingDetails.time = timer6;
  time.html(bookingDetails.time);
});

time7.on('click', function() {
  bookingDetails.time = timer7;
  time.html(bookingDetails.time);
});
time8.on('click', function() {
  bookingDetails.time = timer8;
  time.html(bookingDetails.time);
});

//setting the date
var date = $('#date');
var setDate = $('#setDate');

setDate.on('change', function() {
  bookingDetails.date = setDate.val();
  date.html(bookingDetails.date);
});

//date
const dateNow 	= new Date();
const timeDate 	= dateNow.getHours();
const year 		= dateNow.getFullYear();
const month 	= dateNow.getMonth();
date.html(`${timeDate + 1}/${month}/${year}`);

// Set extras
const ovenLabel = $('#l-box-1');
const windowLabel = $('#l-box-2');
const fridgeLabel = $('#l-box-3');

ovenLabel.on('click', () => {
  //bookingDetails.oven = !document.getElementById('box-1').checked;
  //bookingDetails.extras = bookingDetails.oven ? bookingDetails.extras + 17 : bookingDetails.extras - 17;
  getPrice();
});

windowLabel.on('click', () => {
  //bookingDetails.windows = !document.getElementById('box-2').checked;
  //bookingDetails.extras = bookingDetails.windows ? bookingDetails.extras + 67 : bookingDetails.extras - 67;
  getPrice();
});

fridgeLabel.on('click', () => {
  //bookingDetails.fridge = !document.getElementById('box-3').checked;
  //bookingDetails.extras = bookingDetails.fridge ? bookingDetails.extras + 35 : bookingDetails.extras - 35;
  getPrice();
});

//Tell us about your home
var bedroom = $('#counterOne');
var bath 	= $('#counterTwo');
var laundry	= $('#counterThree');
var otherrooms	= $('#counterFour');
var carpet	= $('#counterFive');
var add 	= $('#plus');
var minus 	= $('#minus');
var add1 	= $('#plus1');
var minus1 	= $('#minus1');
var add2 	= $('#plus2');
var minus2 	= $('#minus2');

var add4 	= $('#plus4');
var minus4 	= $('#minus4');

var add5 	= $('#plus3');
var minus5 	= $('#minus3');

var total = $('#amount');

add.on('click', ()=>{
  if ($('#counterOne').val() >= 6) {
    bedroom.val(5);
  }
  bookingDetails.beds = parseInt(bedroom.val()) + 1;
  //$('#counterOne').val(bookingDetails.beds);
  //alert($('#counterOne').val());  
  getPrice();
});

minus.on('click', function() {
	if ($('#counterOne').val() <= 1) {
		bedroom.val(2);
	}
	bookingDetails.beds = parseInt(bedroom.val()) - 1;
	//$('#counterOne').val(bookingDetails.beds);
	//alert($('#counterOne').val());
	getPrice();
});




add1.on('click', function() {
	if ($('#counterTwo').val() >= 5) {
		bath.val(4);
	}
	bookingDetails.baths = parseInt(bath.val()) + 1;
	console.log(bookingDetails.baths);
	getPrice();
});

minus1.on('click', function() {
  if ($('#counterTwo').val() <= 1) {
    bath.val(2);
  }
  bookingDetails.baths = parseInt(bath.val()) - 1;
  getPrice();
});



add2.on('click', ()=>{
  if ($('#counterThree').val() >= 6) {
    laundry.val(5);
  }
  bookingDetails.laundry = parseInt(laundry.val()) + 1;
  //$('#counterThree').val(bookingDetails.laundry);
  //alert($('#counterThree').val());  
  getPrice();
});

minus2.on('click', function() {
	if ($('#counterThree').val() <= 1) {
		laundry.val(1);
	}
	bookingDetails.laundry = parseInt(laundry.val()) - 1;
	//$('#counterThree').val(bookingDetails.laundry);
	//alert($('#counterOne').val());
	getPrice();
});




add4.on('click', ()=>{
	if ($('#counterFour').val() >= 6) {
		otherrooms.val(5);
	}
	bookingDetails.otherrooms = parseInt(otherrooms.val()) + 1;
	//$('#counterFour').val(bookingDetails.beds);
	//alert($('#counterFour').val());  
	getPrice();
});

minus4.on('click', function() {
	if ($('#counterFour').val() <= 0) {
		otherrooms.val(1);
	}
	bookingDetails.otherrooms = parseInt(otherrooms.val()) - 1;
	//$('#counterOne').val(bookingDetails.beds);
	//alert($('#counterFour').val());
	getPrice();
});

add5.on('click', ()=>{
	
  if ($('#counterFive').val() >= 6) {
	
    carpet.val(5);
  }
  bookingDetails.carpet = parseInt(carpet.val()) + 1;
  getPrice();
});

minus5.on('click', function() {
	
	if ($('#counterFive').val() <= 1) {
		
		carpet.val(1);
	}
	bookingDetails.carpet = parseInt(carpet.val()) - 1;
	getPrice();
});




$('#submit').on('click', () => {
  makeBooking();
});

function getPrice() 
{
	//alert(bookingDetails.type);
	//type 	= bookingDetails.type == 'Move' ? 'Deep' : bookingDetails.type;

	$('#bedrooms_txt').html('');
	$('#otherrooms_li').hide();
	$('#otherrooms_txt').html('');
	$('#laundry_li').hide();
	$('#laundry_txt').html('');
	$('#cleanoven_li').hide();
	$('#cleanoven_txt').html('');	
	$('#cleanwindows_li').hide();
	$('#cleanwindows_txt').html('');
	$('#cleanfridge_li').hide();
	$('#cleanfridge_txt').html('');
	$('#pets_li').hide();
	$('#pets_txt').html('');
	$('#discount_txt').html('');
	$('#discount_li').hide();
	$('#tax_txt').html('');
	$('#tax_li').hide();	
	$('#timetxt').hide();
	$('#otherorderli').hide();
	$('#otherordertxt').html('');
	
	type 	= bookingDetails.type;	
	nBeds 	= pricing[bookingDetails.beds];
	nBaths 	= bookingDetails.baths;
	
	//console.log('type ::::: '+type);
	var x 	= bookingDetails.beds;
	var y	= nBaths;
	var totalamt = 0;

	var otherordertxt = '';
	var xx = 1;//0.9175;
	var discnt = 0;	


	
	if(type == 'Standard'){
		switch(x){
			case 1:
				switch(y){
					case 1:totalamt = 115.79;break;
					case 2:totalamt = 120.38;break;
				}
				break;
			case 2:
				switch(y){
					case 1:totalamt = 143.51;break;
					case 2:totalamt = 157.40;break;
					case 3:totalamt = 161.96;break; 
				}			
				break;
			case 3:
				switch(y){
					case 1:totalamt = 175.83;break;
					case 2:totalamt = 185.10;break;
					case 3:totalamt = 194.30;break;
					case 4:totalamt = 198.90;break;
				}			
				break;
			case 4:
				switch(y){
					case 1:
					case 2:totalamt = 208.20;break;
					case 3:totalamt = 217.40;break;
					case 4:totalamt = 226.61;break;
					case 5:totalamt = 229.50;break;
				}			
				break;
			case 5:
				switch(y){
					case 1:
					case 2:totalamt = 231.30;break;
					case 3:totalamt = 240.50;break;
					case 4:totalamt = 247.90;break;
					case 5:totalamt = 249.80;break;
				}			
				break;
			case 6:
				switch(y){
					case 1:
					case 2:totalamt = 254.40;break;
					case 3:totalamt = 263.70;break;
					case 4:totalamt = 273.00;break;
					case 5:totalamt = 282.10;break;
				}			
				break;
		}
	}
	else{ //if(type == 'Deep')
		switch(x){
			case 1:
				switch(y){
					case 1:totalamt = 138.92;break;
					case 2:totalamt = 143.50;break;
				}
				break;
			case 2:
				switch(y){
					case 1:totalamt = 161.94;break;
					case 2:totalamt = 171.18;break;
					case 3:totalamt = 180.51;break;
				}			
				break;
			case 3:
				switch(y){
					case 1:totalamt = 189.69;break;
					case 2:totalamt = 198.89;break;
					case 3:totalamt = 208.20;break;
					case 4:totalamt = 212.77;break;
				}			
				break;
			case 4:
				switch(y){
					case 1:
					case 2:totalamt = 221.99;break;
					case 3:totalamt = 231.30;break;
					case 4:totalamt = 240.50;break;
					case 5:totalamt = 243.25;break;
				}			
				break;
			case 5:
				switch(y){
					case 1:
					case 2:totalamt = 245.12;break;
					case 3:totalamt = 254.34;break;
					case 4:totalamt = 261.71;break;
					case 5:totalamt = 263.60;break;
				}			
				break;
			case 6:
				switch(y){
					case 1:
					case 2:totalamt = 268.30;break;
					case 3:totalamt = 277.55;break;
					case 4:totalamt = 286.72;break;
					case 5:totalamt = 295.90;break;
				}			
				break;
		}		
	}
	$('#housesizetxt').html(''+x+' bedrooms / '+y+' bath');
	
	if(bookingDetails.otherrooms>0){
		totalamt += xx*20*bookingDetails.otherrooms;
		$('#housesizetxt').html(''+x+' bedrooms / '+y+' bath and '+bookingDetails.otherrooms+' Other Rooms');
	}
	

	
	//$('#bedrooms_txt').html('$'+(totalamt).toFixed(2));
	//$('#bedrooms_li').show();
	//totalamt = xx*totalamt;
	if(bookingDetails.frequency == 'weekly'){
		discnt 		= 0.10*totalamt;
		$('#often').html('Weekly');
		$('#discount_li').show();
	}
	else if(bookingDetails.frequency == 'biWeekly'){
		discnt 		= 0.05*totalamt;
		$('#often').html('Bi-Weekly');
		$('#discount_li').show();
	}
	else if(bookingDetails.frequency == 'monthly'){
		discnt 		= 0.02*totalamt;
		$('#often').html('Monthly'); 
		$('#discount_li').show();
	}
	discnt		= discnt.toFixed(2);
	//totalamt	= totalamt-discnt;
	$('#discount_txt').html('-$'+discnt);
	

	
	
	
	if($('#box-1').prop("checked") == true){
		totalamt += xx*25;
		otherordertxt = 'Clean Oven';
	}
	
	if($('#box-2').prop("checked") == true){
		totalamt += xx*40;
		if(otherordertxt != ''){
			otherordertxt = otherordertxt+', Windows';
		}
		else{
			otherordertxt = 'Clean Windows';
		}
	}
	
	if($('#box-3').prop("checked") == true){
		totalamt += xx*25;
		if(otherordertxt != ''){
			otherordertxt = otherordertxt+', Fridge';
		}
		else{
			otherordertxt = 'Clean Fridge';
		}
	}
	
	if(bookingDetails.laundry>0){
		totalamt += xx*15*bookingDetails.laundry;
		if(otherordertxt != ''){
			otherordertxt = otherordertxt+' and '+bookingDetails.laundry+' Laundry';
		}
		else{
			otherordertxt = bookingDetails.laundry+' Laundry';
		}		
	}

	if(bookingDetails.carpet>0){
		switch(bookingDetails.carpet){
			case 1:totalamt += xx*65;break;
			case 2:totalamt += xx*80;break;
			case 3:totalamt += xx*120;break;
			case 4:totalamt += xx*160;break;
			case 5:totalamt += xx*200;break;
			case 6:totalamt += xx*240;break;
		}
		txt = bookingDetails.carpet+' Room ';
		if(bookingDetails.carpet > 1){
			txt = bookingDetails.carpet+' Rooms & Hallway';
		}
		
		if(otherordertxt != ''){
			otherordertxt = otherordertxt+'<br>'+txt;
		}
		else{
			otherordertxt = txt;
		}
	}
	
	pets = $("input[name='pets']:checked").val();	
    if(pets == 1){
		totalamt += xx*15;
	}	

	if(otherordertxt != ''){
		$('#otherorderli').show();
		$('#otherordertxt').html(otherordertxt);
	}




	




	









	
	
	
	$('#stotal_li').show();
	$('#stotal_txt').html('$'+totalamt.toFixed(2));	
	
	
	totalamt 	= totalamt-discnt;
	stax = 0.0825*totalamt;
	totalamt = (totalamt+stax).toFixed(2);
	
	$('#tax_li').show();
	$('#tax_txt').html('$'+stax.toFixed(2));
	
	
	
	$('#stripe_amount').val(totalamt);
	$('#amount').html('$'+totalamt);
	
}




function getDiscount() {
  return bookingDetails.total - ((discounts[bookingDetails.frequency]*bookingDetails.total) / 100);
}

function makeBooking() {
	console.log(bookingDetails);
	$.ajax({
		url: 'submit.php',
		type: 'post',
		dataType: 'json',
		contentType: 'application/json',
		success: function (data) {
			console.log(data);
		},
		data: JSON.stringify(bookingDetails)
	});
}

function hearsourcefn(){
	$('#others_info').hide();
	if($('#hearsource').val() == 'Other'){
		$('#others_info').show();
	}
	
}

getPrice();
getPrice();