<?php
	http_response_code(200);
	echo json_encode(array("message"=>"Booking successful","error"=>"Mail not sent"));
	exit;
  include_once 'Admin/settings.php';

  // Get posted data
  $data = json_decode(file_get_contents("php://input"));
  $bookingNo = substr($data->name, 0, 3).mt_rand(10000, 99999);


  // SQL Query to insert values from book
  $query = "INSERT into 
              bookings 
            SET
              Id=null,
              bookingNo=:bookNo,
              clientName=:name,
              clientMail=:mail,
              clientPhone=:phone,
              clientAddress=:address,
              clientZip=:zip,
              cleanType=:type,
              cleanFrequency=:frequency,
              cleanDate=:date,
              cleanTime=:time,
              noBeds=:beds,
              noBaths=:baths,
              fridge=:fridge,
              cabinet=:cabinets,
              laundry=:laundry,
              windows=:windows,
              oven=:oven,
              total=:price,
              dateCreated=Now()
          ";
  
  // Prepare Query
  $stmt = $con->prepare($query);

  // Sanitize Values
  $data->name=htmlspecialchars(strip_tags($data->name));
  $data->address=htmlspecialchars(strip_tags($data->address));
  $data->fridge = (int) $data->fridge;
  $data->cabinets = (int) $data->cabinets;
  $data->windows = (int) $data->windows;
  $data->oven = (int) $data->oven;
  $date = DateTime::createFromFormat( 'H:i A', $data->time);
  $data->time = $date->format( 'H:i:s');

  // Bind Parameters to query
  $stmt->bindParam(":bookNo", $bookingNo);
  $stmt->bindParam(":name", $data->name);
  $stmt->bindParam(":mail", $data->mail);
  $stmt->bindParam(":phone", $data->phone);
  $stmt->bindParam(":address", $data->address);
  $stmt->bindParam(":zip", $data->zip);
  $stmt->bindParam(":type", $data->type);
  $stmt->bindParam(":frequency", $data->frequency);
  $stmt->bindParam(":date", $data->date);
  $stmt->bindParam(":time", $data->time);
  $stmt->bindParam(":beds", $data->beds);
  $stmt->bindParam(":baths", $data->baths);
  $stmt->bindParam(":fridge", $data->fridge);
  $stmt->bindParam(":cabinets", $data->cabinets);
  $stmt->bindParam(":laundry", $data->laundry);
  $stmt->bindParam(":windows", $data->windows);
  $stmt->bindParam(":oven", $data->oven);
  $stmt->bindParam(":price", $data->price);

  $successState = $stmt->execute() || false;
  
  if ($successState) {
    // Set mail Variables
    $subject = "Your Booking Order";
    $userMail = $data->mail;
    $userName = $data->name;
    
    // Prepare mail message
    $message = file_get_contents('snipnetAPI/template/buyer-trans-email.htm');

    // Parse message
    $message = str_replace('%receiver_name%', $userName, $message);
    $message = str_replace('%type%', $userName, $message);
    $message = str_replace('%frequency%', $userName, $message);
    $message = str_replace('%date%', $data->date, $message);
    $message = str_replace('%Time%', $data->time, $message);
    $message = str_replace('%beds%', $data->beds, $message);
    $message = str_replace('%baths%', $data->baths, $message);
    $message = str_replace('%fridge%', $data->fridge, $message);
    $message = str_replace('%cabinet%', $data->cabinets, $message);
    $message = str_replace('%laundry%', $data->laundry, $message);
    $message = str_replace('%window%', $data->windows, $message);
    $message = str_replace('%oven%', $data->oven, $message);
    $message = str_replace('%bookno%', $bookingNo, $message);
    $message = str_replace('%total%', $data->price, $message);

    // echo $message;

    // Send mail
    if(mailer($userMail, $userName, $message, $subject)){
      http_response_code(200);
      echo json_encode(
        array(
          "message"=>"Booking successful"
        )
      );
    } else {
      http_response_code(200);
      echo json_encode(
        array(
          "message"=>"Booking successful",
          "error"=>"Mail not sent"
        )
      );
    }
  } else {
    http_response_code(501);
    echo json_encode(
      array(
        "error"=>"Booking failed"
      )
    );
  }
  
?>