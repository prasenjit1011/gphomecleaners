<?php
session_start();
//echo '-------'.$_SESSION['msg'].'+++++++++++++++';exit;
if(!isset($_SESSION['successMessage'])){
	//header("Location:https://gphomecleaners.com/mybooking.php");
	//exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Grand Praire Home Cleaners — Booking</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/font-awesome.min.css" />
  <!-- Simple Line Font -->
  <link rel="stylesheet" href="css/pe-icon-7-stroke.css" />

  <link rel="stylesheet" href="css/datepicker.min.css" />
  <!-- Main CSS -->
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <!--============================= HEADER =============================-->
  <header class="header-style2 fixed-top">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <a href="index.html"><img src="images/logo.png" alt="logo" height="60"/></a><span>469-251-7682</span>
        </div>
      </div>
    </div>
  </header>
  <!--//END HEADER -->
  <!--============================= BOOKING SECTION =============================-->
  <section class="booking-details center-block main-block">
    <div class="container">
      <div class="row">
        <div class="col-md-12"><br>
          <h2>Payment Completed Successful</h2>
          <h6>
            Back to <a href="/">Home Page</a>.
          </h6>
        </div>
      </div>
	  <?php /*
      <div class="row mt-5">
        <div class="col-md-12 set-sm-fit mb-4">
            <div class="preference-title">
              <h4>Cleaning Preferences Deatils</h4>
            </div>
			<!-- preferences Wrap -->
            <div class="preferences">
			
				<?php	
					$msg = $_REQUEST['successMessage'];
					echo '<h3>'.base64_decode($msg).'</h3>';
				?>
            </div>
		</div>

      </div>*/ ?>
    </div>
  </section>
  <!--//END BOOKING SECTION -->

  <!-- jQuery, Bootstrap JS. -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Slick JS -->
  <script src="js/slick.min.js"></script>
  <!-- Date Picker JS -->
  <script src="js/datepicker.min.js"></script>
  <!-- Main JS -->
  <script src="js/script.js"></script>
  <!-- booking JS -->
  <script src="js/booking.js"></script>
</body>
</html>